# Practical Work: Introduction to Docker

This practical work aims to introduce you to Docker, a crucial tool in the development and operations (DevOps) world, especially within the context of security (DevSecOps).

## Objectives

- Understand the basics of Docker.
- Learn how to launch and manage containers.
- Explore advanced features such as port binding and volume mounting.

## Prerequisites

- Docker installed on your machine. If Docker is not installed yet, please follow the official instructions at [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/).

## Part 1: Hello World

1. **Launching a Hello World Container**

   Open a terminal and execute the following command to download and run a container that executes a simple script displaying "Hello World".

   ```bash
   docker run hello-world
   ```

This command downloads a lightweight image and runs a container based on that image. The output provides information on what Docker did behind the scenes.

## Part 2: Interacting with a Container

1. **Launch an Ubuntu Container in Interactive Mode**

   Now, let's run an Ubuntu container and interact with it through a bash terminal.

   ```bash
   docker run -it ubuntu bash
   ```

You are now inside a bash shell within the container. Try executing some commands (`ls`, `cat /etc/os-release`, etc.), then exit by typing exit.

## Part 3: Port Binding and Volume Mounting

1. **Launching a Nginx Web Server**

We will launch a Nginx web server and make it accessible on port 8080 of your host machine.

   ```bash
   docker run -p 8080:80 -d nginx
   ```

You can now access the Nginx server from your browser by going to http://localhost:8080.

2. **Mounting a Volume**

Create a html folder on your host machine and add an index.html file with content of your choice. Then, mount this folder into a Nginx container and expose it on a different port.

   ```bash
   docker run -v $(pwd)/html:/usr/share/nginx/html -p 8081:80 -d nginx
   ```
Replace `$(pwd)/html` with the absolute path to your html folder if necessary. You can access your custom page by going to http://localhost:8081.

## Part 4: Environment Variables and Execution Command

1. Launching a MySQL Container

Use the following command to launch a MySQL container, setting an environment variable for the root password.

   ```bash
   docker run --name mysql-test -e MYSQL_ROOT_PASSWORD=mySecurePassword -d mysql
   ```

Note that the container is launched in detached mode, and you've specified a root password via `-e MYSQL_ROOT_PASSWORD=mySecurePassword`.

2. Connecting to the MySQL Container

To connect to the running MySQL container, you can use the docker exec command to launch a MySQL client within the container.

   ```bash
   docker exec -it mysql-test mysql -p
   ```

## Part 5: Running Containers with Specific User (-u) and Work Directory (-w)

1. Running a Container as a Specific User

You can specify a user ID (UID) or name to run commands in a container with non-default permissions. This is particularly useful for running services that don't require root privileges, enhancing security.

   ```bash
   docker run -u 1000 ubuntu whoami
   ```

This command runs the `whoami` command in an Ubuntu container, simulating execution by a user with UID 1000. Replace 1000 with the desired UID.

2. Specifying the Working Directory (-w) inside a Container

The `-w` flag allows you to set the working directory for the command executed within the container.

   ```bash
   docker run -w /app ubuntu sh -c "pwd && ls"
   ```

This command sets `/app` as the working directory and lists its contents. If the directory does not exist, Docker will create it for you, but it will be empty.

## Docker Networking Example: Web Application and MySQL Database

1. Create a Docker Network

First, we create a custom Docker network named `my-app-network`. This network will provide a secure communication channel between the web application and the MySQL database.

   ```bash
   docker network create my-app-network
   ```

2. Run a MySQL Container

Launch a MySQL container and connect it to the `my-app-network`. We'll name the container `my-mysql` and set a root password (in a real scenario, use a more secure password).

   ```bash
docker run --name my-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -e MYSQL_DATABASE=mydb --network=my-app-network -d mysql:5.7
   ```

This command does the following:

- Starts a MySQL container with the name my-mysql.
- Sets the environment variable MYSQL_ROOT_PASSWORD to my-secret-pw.
- Creates a database named mydb.
- Attaches the container to the my-app-network.
- Uses the mysql:5.7 image from Docker Hub.

3. Run PHPmyAdmin

To launch a phpMyAdmin container connected to your MySQL container on the `my-app-network`, use the following command:

   ```bash
   docker run --name my-phpmyadmin -d --network=my-app-network -e PMA_HOST=my-mysql -p 8080:80 phpmyadmin/phpmyadmin
   ```

This command does the following:

- Starts a container with the name `my-phpmyadmin`.
- Uses the `my-app-network` to allow phpMyAdmin to communicate with the MySQL container (`my-mysql`).
- Sets the environment variable `PMA_HOST` to `my-mysql`, indicating to phpMyAdmin the MySQL host to connect to.
- Maps port 8080 of the host to port 80 in the container, making phpMyAdmin accessible via `http://localhost:8080`.

4. Accessing phpMyAdmin

To access phpMyAdmin in your browser:

Open a web browser and go to `http://localhost:8080`. You should see the phpMyAdmin login interface. Use `root` as the username and `my-secret-pw` as the password (or the credentials you configured for your MySQL container).

Once connected to phpMyAdmin, you can:

- Verify that the mydb database exists.
- Inspect the tables and data to ensure they match what your PHP application expects to find.
- Execute SQL queries directly from the phpMyAdmin interface to test communication with the database.

## Docker Commands and Management

1. Listing Running Containers

- `docker ps`: Lists all currently running containers. It shows a snapshot of the containers that are currently active.

2. Listing All Containers

- `docker ps -a`: Lists all containers, including those that are not currently running. This is useful for seeing a history of containers you've run.

3. Managing Docker Images

- `docker images`: Lists all Docker images available on your machine. Images are the base templates from which containers are created.

4. Removing Docker Containers and Images

- Remove containers and images: Removing unused containers and images can help free up disk space. Use `docker rm` for containers and `docker rmi` for images.

5. Viewing Logs
- `docker logs <container_name_or_id>`: Use this command to view the logs of a container. This is particularly useful for debugging issues with your containers.

6. Deleting a Docker Network

To delete the custom Docker network named my-app-network, use the following command:

   ```bash
docker network rm my-app-network
   ```
   
This command removes the network, freeing up resources on your system. Ensure that no running containers are connected to the network before attempting to remove it. If necessary, stop and remove those containers using docker stop <container_name> and docker rm <container_name>.

   ```bash
   docker logs mysql
   ```

## Dockerfile

A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image. Using `docker build` users can create an automated build that executes several command-line instructions in succession.

### Use Cases for Dockerfile:

- **Building Custom Images:** When you need a custom Docker image tailored to your application, you start with a Dockerfile. It defines the base image, software packages, scripts, environment variables, and other components needed in your container.
- **Automation and Repeatability:** Dockerfiles allow you to automate the setup of your Docker container environment. This ensures that you can recreate your Docker container from scratch anytime, leading to consistent environments.
- **Simplicity for Single-Container Applications:** For simple applications that require only a single container, a Dockerfile may be all you need.

Here is a Dockerfile example that build a Flask application :

   ```bash
   # Use an official Python runtime as a parent image
   FROM python:3.8-alpine

   # Set the working directory in the container
   WORKDIR /app

   # Copy the current directory contents into the container at /app
   COPY . /app

   # Install any needed packages specified in requirements.txt
   RUN pip install --no-cache-dir -r requirements.txt

   # Make port 80 available to the world outside this container
   EXPOSE 80

   # Define environment variable
   ENV NAME World

   # Run app.py when the container launches
   CMD ["python", "app.py"]
   ```

### Building an Image from a Dockerfile

First, ensure you are in the directory containing your Dockerfile. If your Dockerfile is named Dockerfile, the command to build an image is straightforward:

   ```bash
   docker build -t my-flask-app .
   ```

- `-t my-flask-app` tags (names) your new image as `my-flask-app`.
- The `.` at the end tells Docker to use the current directory as the build context, including the Dockerfile and any files it references.

### Tagging an image

Docker tags are a way to give versions or specific identifiers to your Docker images. Here’s how to add a tag to an image:

   ```bash
   docker tag my-flask-app my-flask-app:v1
   ```

This command tags the `my-flask-app` image with a version identifier `v1`, making it `my-flask-app:v1`. This is useful for version control and maintaining multiple releases of the same application.

### Running a Container from Your Image

To run a container from your newly created image:

   ```bash
   docker run -p 5000:80 my-flask-app
   ```
- `-p 5000:80` maps port 80 of the container to port 5000 on your host machine. This allows you to access your Flask app at `http://localhost:5000`.

### Pushing Your Image to a Registry

Before pushing, ensure you are logged in to Docker Hub or another registry:

   ```bash
   docker login
   ```

Then push your image:

   ```bash
   docker push my-flask-app:v1
   ```

This command uploads your image to the Docker registry, allowing others to pull and use your image. Replace `my-flask-app:v1` with your Docker Hub username, like `username/my-flask-app:v1`, if you’re pushing to Docker Hub.

## Docker-Compose

Docker Compose is a tool for defining and running multi-container Docker applications. With a YAML file (`docker-compose.yml`), you can configure your application’s services, networks, and volumes, and manage the whole lifecycle with simple commands.

### Use Cases for Docker Compose:

- **Orchestrating Multi-Container Applications:** Docker Compose excels at managing applications comprised of multiple containers that need to work together.
- **Development Environments:** It's particularly useful for local development and testing, allowing you to spin up and tear down environments that mimic production setups.
- **CI/CD Pipelines:** Docker Compose can be used in Continuous Integration and Continuous Deployment pipelines to ensure applications are tested in an environment that closely matches the production setup.

### Example:

```yaml
version: '3'
services:
  web:
    build: ./web
    ports:
      - "5000:5000"
    volumes:
      - .:/app
    depends_on:
      - redis
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
  nginx:
    image: "nginx:alpine"
    ports:
      - "8080:80"
    volumes:
      - ./static:/usr/share/nginx/html:ro
    depends_on:
      - web
```

- Version: Specifies the version of the Docker Compose file format. `version: '3'` is commonly used and supports most features.
- Services: Defines the containers you want to run. In this example, there are three services: `web`, `redis`, and `nginx`.
   - The web service builds from a Dockerfile located in the ./web directory, maps port 5000 on the host to port 5000 in the container, mounts the current directory (`.`) to `/app` in the container for live code reloading, and sets an environment variable.
   - The `redis` service uses the official Redis image from Docker Hub and doesn't require additional configuration for this simple setup.
   - The `nginx` service uses the official Nginx image, maps port 8080 on the host to port 80 in the container, serves static content from the `./static` directory, and ensures it starts only after the `web` service is up.
- Volumes: Mounts host paths or named volumes into services, allowing for data persistence or sharing code between the host and container.
- Depends_on: Specifies dependency between services, ensuring services start in the correct order.

### Running the Application:

To start the application using this `docker-compose.yml`, navigate to the directory containing the file and run:

   ```bash
   docker-compose up --build
   ```

This command builds the images (if necessary) and starts the containers. To access the application, visit http://localhost:5000 for the Flask app and http://localhost:8080 for static content served by Nginx.

To stop and remove the containers, 

Build the image and run the container

Now, let's build the image and run the container:

docker compose build
docker compose up -d flaskapp
docker ps -a
s, and volumes created by `docker-compose up`, run:

   ```bash
   docker-compose down
   ```

This `docker-compose.yml` example demonstrates the simplicity and power of Docker Compose for local development and testing of multi-container applications.

## Running Python fullstack REST API app with Docker and Docker Compose

Before we start, here is a simple schema explaining the app's architecture.

![alt text](images/architecture.jpg)

Technologies :

- Next.js 14 (TypeScript)
- Tailwind CSS
- Flask (Python) + SQLAlchemy (ORM)
- PostgreSQL

### Database

We will use Postgres but not install it on our machine. Instead, we will use Docker to run it in a container. This way, we can easily start and stop the database without installing it on our machine.

Open the file compose.yml and add the following content:

```yaml
services:
  db:
    container_name: db
    image: postgres:13
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: postgres
    ports:
      - 5432:5432
    volumes:
      - pgdata:/var/lib/postgresql/data

volumes:
  pgdata: {}
```

and type in your terminal

   ```bash
   docker compose up -d
   ```

This will pull the Postgres image from Docker Hub and start the container. The `-d` flag means that the container will run in detached mode so we can continue to use the terminal.

Check if the container is running:

   ```bash
   docker ps
   ```

Step into the db container

   ```bash
   docker exec -it db psql -U postgres
   ```

Now that you are in the Postgres container, you can type:

   ```bash
   \l
   \dt
   ```

And you should see no relations.
You can leave the tab open. We will use it later.

### Backend

The first step is done. Now, we will create the backend. We will use Go and Mux.

Jump to the folder called backend in `fullstack/` :

   ```bash
   cd fullstack/backend
   ```

#### requirements.txt file 

You can see the `requirements.txt` that contains all the dependencies of the project :

   ```bash
   flask
   psycopg2-binary
   Flask-SQLAlchemy
   Flask-CORS
   ```

- `flask` is the Python web framework we are gonna use.

- `psycopg2-binary` is the driver to make the connection with the Postgres database.

- `Flask-SQLAlchemy` is the ORM to make the queries to the database.

- `Flask-CORS` is a Flask extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.

#### app.py file

The app.py file is the main file of the application: it contains all the endpoints and the logic of the application.

   ```python
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS  
from os import environ

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes
app.config['SQLALCHEMY_DATABASE_URI'] = environ.get('DATABASE_URL')
db = SQLAlchemy(app)

class User(db.Model):
  __tablename__ = 'users'
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(80), unique=True, nullable=False)
  email = db.Column(db.String(120), unique=True, nullable=False)

  def json(self):
    return {'id': self.id,'name': self.name, 'email': self.email}

db.create_all()

#create a test route
@app.route('/test', methods=['GET'])
def test():
  return make_response(jsonify({'message': 'test route'}), 200)

# create a user
@app.route('/api/flask/users', methods=['POST'])
def create_user():
  try:
    data = request.get_json()
    new_user = User(name=data['name'], email=data['email'])
    db.session.add(new_user)
    db.session.commit()

    return jsonify({
        'id': new_user.id,
        'name': new_user.name,
        'email': new_user.email
    }), 201
  except Exception as e:
      return make_response(jsonify({'message': 'error creating user', 'error': str(e)}), 500)

# get all users
@app.route('/api/flask/users', methods=['GET'])
def get_users():
  try:
    users = User.query.all()
    users_data = [{'id': user.id, 'name': user.name, 'email': user.email} for user in users]
    return jsonify(users_data), 200
  except Exception as e:
    return make_response(jsonify({'message': 'error getting users', 'error': str(e)}), 500)

# get a user by id
@app.route('/api/flask/users/<int:id>', methods=['GET'])
def get_user(id):
  try:
    user = User.query.filter_by(id=id).first()
    if user:
      return make_response(jsonify({'user': user.json()}), 200)
    return make_response(jsonify({'message': 'user not found'}), 404)
  except Exception as e:
    return make_response(jsonify({'message': 'error getting user', 'error': str(e)}), 500)

# update a user
@app.route('/api/flask/users/<int:id>', methods=['PUT'])
def update_user(id):
  try:
    user = User.query.filter_by(id=id).first()
    if user:
      data = request.get_json()
      user.name = data['name']
      user.email = data['email']
      db.session.commit()
      return make_response(jsonify({'message': 'user updated'}), 200)
    return make_response(jsonify({'message': 'user not found'}), 404)
  except Exception as e:
    return make_response(jsonify({'message': 'error updating user', 'error': str(e)}), 500)

# delete a user
@app.route('/api/flask/users/<int:id>', methods=['DELETE'])
def delete_user(id):
  try:
    user = User.query.filter_by(id=id).first()
    if user:
      db.session.delete(user)
      db.session.commit()
      return make_response(jsonify({'message': 'user deleted'}), 200)
    return make_response(jsonify({'message': 'user not found'}), 404)
  except Exception as e:
    return make_response(jsonify({'message': 'error deleting user', 'error': str(e)}), 500)
   ```

We are importing:

   - Flask as a framework
   - request to handle the HTTP
   - jsonify to handle the json format, not native in Python
   - make_response to handle the HTTP responses
   - flask_sqlalchemy to handle the db queries
   - environ to handle the environment variables
   - CORS to handle the Cross Origin Resource Sharing

Then we are creating the Flask app, configuring the database bu setting an environment variable called 'DATABASE_URL'. We will set it later in the docker-compose.yml file.

Then we are creating a User class with an id, a name and an email. the id will be autoincremented automatically by SQLAlchemy when we will create the users. the tablename = 'users' line is to define the name of the table in the database
We are creating Flask app, configuring the database bu setting an environment variable called 'DB_URL'. We will set it later in the docker-compose.yml file.

Then we are creating a User class with an id, a name and an email. the id will be autoincremented automatically by SQLAlchemy when we will create the users. the tablename = 'users' line is to define the name of the table in the database

An important line is db.create_all(). This will synchronize the database with the model defined, for example creating an "users" table.

Then we have 6 endpoints

- test: just a test route
- create a user: create a user with a name and an email
- get all users: get all the users in the database
- get one user: get one user by id
- update one user: update one user by id
- delete one user: delete one user by id

All the routes have error handling, for example if the user is not found, we will return a 404 HTTP response.

#### Dockerize the Python app

The Dockerfile file  in `fullstack/backend` is the file that will be used to containerize the Flask application

```bash
FROM python:3.6-slim-buster

WORKDIR /app

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . .

EXPOSE 4000

CMD [ "flask", "run", "--host=0.0.0.0", "--port=4000"]
```

`FROM` sets the base image to use. In this case we are using the python 3.6 slim buster image

`WORKDIR` sets the working directory inside the image

`COPY` requirements.txt ./ copies the requirements.txt file to the working directory

`RUN` pip install -r requirements.txt installs the requirements

`COPY . .` copies all the files in the current directory to the working directory

`EXPOSE 4000` exposes the port 4000

`CMD [ "flask", "run", "--host=0.0.0.0", "--port=4000"]` sets the command to run when the container starts

#### Update the compose.yml file

Update the `compose.yml` by adding the flaskapp service.

```yaml
services:
  flaskapp:
    container_name: flaskapp
    image: flaskapp:1.0.0
    build:
      context: backend
      dockerfile: Dockerfile
    ports:
      - '4000:4000'
    restart: always
    environment:
      - DATABASE_URL=postgresql://postgres:postgres@db:5432/postgres
    depends_on:
      - db
  db:
    container_name: db
    image: postgres:13
    restart: always
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: postgres
    ports:
      - 5432:5432
    volumes:
      - pgdata:/var/lib/postgresql/data

volumes:
  pgdata:
```

#### Build the image and run the container

Now, jump to the fullstack directory and let's build the image and run the container:

   ```bash
   docker compose build
   docker compose up -d flaskapp
   docker ps -a
   ```

#### Test the backend

We are now ready to test the backend.

You can use Postman or any other tool to make HTTP requests.

**get all**

Yoy can get all the users, but making a GET request to `http://localhost:4000/api/flask/users`

**Create a new user**

You can create a new user, but making a POST request to `http://localhost:4000/api/flask/users`

You can create 2 more users, to have a total of 3 users.

Let's check the database:

   ```bash
   docker exec -it db psql -U postgres
   select * from users;
   ```

**update and delete a user**

You can update a user, but making a PUT request to `http://localhost:4000/api/flask/users/3`

You can delete a user, but making a DELETE request to `http://localhost:4000/api/flask/users/3`

#### Frontend

Now that we have the backend up and running, we can proceed with the frontend.

We will use Next.js 14 with TypeScript and Tailwind.

From the `fullstack folder` of the project, run this command:

npx create-next-app@latest --no-git

We use the --no-git flag because we already initialized a git repository at the project's root.

As options:

   - What is your project named? `frontend`
   - TypeScript? `Yes`
   - EsLint? `Yes`
   - Tailwind CSS? `Yes`
   - Use the default directory structure? `Yes`
   - App Router? `No` (not needed for this project)
   - Customize the default import alias? `No`

This should create a new Next.js project in about one minute.

Step into the frontend folder:

   ```bash
   cd frontend
   ```

Install Axios, we will use it to make HTTP requests (be sure to be in the `frontend` folder):

   ```bash
   npm i axios
   ```

Before we proceed, try to run the project:

   ```bash
   npm run dev
   ```

And open your browser at `http://localhost:3000`. You should see the default Next.js page.

#### Modify the styles/global.css file

In the `fullstack/frontend/src/styles/globals.css` file, replace the content with this one (to avoid some problems with Tailwind):

```js
@tailwind base;
@tailwind components;
@tailwind utilities;

:root {
  --foreground-rgb: 0, 0, 0;
  --background-start-rgb: 214, 219, 220;
  --background-end-rgb: 255, 255, 255;
}

body {
  color: rgb(var(--foreground-rgb));
  background: linear-gradient(to bottom, transparent, rgb(var(--background-end-rgb))) rgb(var(--background-start-rgb));
}

```

####  Create new components

   ```bash
   cd src
   mkdir components
   touch CardComponent.tsx UserInterface.tsx
   ```

In the `/frontend/src` folder, create a new folder called `components` and inside it create a new file called `CardComponent.tsx` and add the following content:

```js
import React from 'react';

interface Card {
  id: number;
  name: string;
  email: string;
}

const CardComponent: React.FC<{ card: Card }> = ({ card }) => {
  return (
    <div className="bg-white shadow-lg rounded-lg p-2 mb-2 hover:bg-gray-100">
      <div className="text-sm text-gray-600">Id: {card.id}</div>
      <div className="text-lg font-semibold text-gray-800">{card.name}</div>
      <div className="text-md text-gray-700">{card.email}</div>
    </div>
  );
};

export default CardComponent;
```

#### Create a UserInterface component 

In the `/frontend/src/components` folder, create a file called UserInterface.tsx and add the following content:

```js
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CardComponent from './CardComponent';

interface User {
  id: number;
  name: string;
  email: string;
}

interface UserInterfaceProps {
  backendName: string;
}

const UserInterface: React.FC<UserInterfaceProps> = ({ backendName }) => {
  const apiUrl = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:4000';
  const [users, setUsers] = useState<User[]>([]);
  const [newUser, setNewUser] = useState({ name: '', email: '' });
  const [updateUser, setUpdateUser] = useState({ id: '', name: '', email: '' });

  const backgroundColors: { [key: string]: string } = {
    flask: 'bg-blue-500',
  };

  const buttonColors: { [key: string]: string } = {
    flask: 'bg-blue-700 hover:bg-blue-600',
  };

  const bgColor = backgroundColors[backendName as keyof typeof backgroundColors] || 'bg-gray-200';
  const btnColor = buttonColors[backendName as keyof typeof buttonColors] || 'bg-gray-500 hover:bg-gray-600';

  // Fetch users
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${apiUrl}/api/${backendName}/users`);
        setUsers(response.data.reverse());
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [backendName, apiUrl]);

  // Create a user
  const createUser = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${apiUrl}/api/${backendName}/users`, newUser);
      setUsers([response.data, ...users]);
      setNewUser({ name: '', email: '' });
    } catch (error) {
      console.error('Error creating user:', error);
    }
  };

  // Update a user
  const handleUpdateUser = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      await axios.put(`${apiUrl}/api/${backendName}/users/${updateUser.id}`, { name: updateUser.name, email: updateUser.email });
      setUpdateUser({ id: '', name: '', email: '' });
      setUsers(
        users.map((user) => {
          if (user.id === parseInt(updateUser.id)) {
            return { ...user, name: updateUser.name, email: updateUser.email };
          }
          return user;
        })
      );
    } catch (error) {
      console.error('Error updating user:', error);
    }
  };

  // Delete a user
  const deleteUser = async (userId: number) => {
    try {
      await axios.delete(`${apiUrl}/api/${backendName}/users/${userId}`);
      setUsers(users.filter((user) => user.id !== userId));
    } catch (error) {
      console.error('Error deleting user:', error);
    }
  };

  return (
    <div className={`user-interface ${bgColor} ${backendName} w-full max-w-md p-4 my-4 rounded shadow`}>
      <img src={`/${backendName}logo.svg`} alt={`${backendName} Logo`} className="w-20 h-20 mb-6 mx-auto" />
      <h2 className="text-xl font-bold text-center text-white mb-6">{`${backendName.charAt(0).toUpperCase() + backendName.slice(1)} Backend`}</h2>

      {/* Form to add new user */}
      <form onSubmit={createUser} className="mb-6 p-4 bg-blue-100 rounded shadow">
        <input
          placeholder="Name"
          value={newUser.name}
          onChange={(e) => setNewUser({ ...newUser, name: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />

        <input
          placeholder="Email"
          value={newUser.email}
          onChange={(e) => setNewUser({ ...newUser, email: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />
        <button type="submit" className="w-full p-2 text-white bg-blue-500 rounded hover:bg-blue-600">
          Add User
        </button>
      </form>

      {/* Form to update user */}
      <form onSubmit={handleUpdateUser} className="mb-6 p-4 bg-blue-100 rounded shadow">
        <input
          placeholder="User Id"
          value={updateUser.id}
          onChange={(e) => setUpdateUser({ ...updateUser, id: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />
        <input
          placeholder="New Name"
          value={updateUser.name}
          onChange={(e) => setUpdateUser({ ...updateUser, name: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />
        <input
          placeholder="New Email"
          value={updateUser.email}
          onChange={(e) => setUpdateUser({ ...updateUser, email: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />
        <button type="submit" className="w-full p-2 text-white bg-green-500 rounded hover:bg-green-600">
          Update User
        </button>
      </form>

      {/* Display users */}
      <div className="space-y-4">
        {users.map((user) => (
          <div key={user.id} className="flex items-center justify-between bg-white p-4 rounded-lg shadow">
            <CardComponent card={user} />
            <button onClick={() => deleteUser(user.id)} className={`${btnColor} text-white py-2 px-4 rounded`}>
              Delete User
            </button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserInterface;
```

#### Modify the index.tsx file

Open the `index.tsx` file and replace the content with the following:

```js
import React from 'react';
import UserInterface from '../components/UserInterface';

const Home: React.FC = () => {
  return (
    <main className="flex flex-wrap justify-center items-start min-h-screen bg-gray-100">
      <div className="m-4">
        <UserInterface backendName="flask" />
      </div>
    </main>
  );
};

export default Home;
```

#### Test the frontend

We are now ready to test the frontend.

You can use the UI to insert, update, and delete users.

You can create a user directly from the UI

You can check the updated users in the Postgres database

```bash
docker exec -it db psql -U postgres
select * from users;
```

You can also update a user

And finally, you can delete a user, just by clicking on the "Delete User' button

#### Dockerize the frontend 

Deploy a Next.js app with Docker.

Change the `next.config.mjs` file in the `frontend` folder, replacing it with the following content:

```js
/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'standalone',
};

module.exports = nextConfig;
```

Create a file called `.dockerignore` in the `frontend` folder and add the following content:

```bash
touch .dockerignore Dockerfile
```

```bash
Dockerfile
.dockerignore
node_modules
npm-debug.log
README.md
.next
.git
```
To dockerize the Next.js application, we will use the official Dockerfile provided by Vercel:

Create a file called `Dockerfile` in the frontend folder and add the following content (it's directly from the vercel official docker example)

```bash
FROM node:18-alpine AS base

# Install dependencies only when needed
FROM base AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app

# Install dependencies based on the preferred package manager
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
RUN \
  if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i --frozen-lockfile; \
  else echo "Lockfile not found." && exit 1; \
  fi


# Rebuild the source code only when needed
FROM base AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry during the build.
# ENV NEXT_TELEMETRY_DISABLED 1

RUN yarn build && ls -l /app/.next


# If using npm comment out above and use below instead
# RUN npm run build

# Production image, copy all the files and run next
FROM base AS runner
WORKDIR /app

ENV NODE_ENV production
# Uncomment the following line in case you want to disable telemetry during runtime.
# ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY --from=builder /app/public ./public

# Set the correct permission for prerender cache
RUN mkdir .next
RUN chown nextjs:nodejs .next

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static

USER nextjs

EXPOSE 3000

ENV PORT 3000
# set hostname to localhost
ENV HOSTNAME "0.0.0.0"

# server.js is created by next build from the standalone output
# https://nextjs.org/docs/pages/api-reference/next-config-js/output
CMD ["node", "server.js"]
```

Now, let's update the `compose.yaml` file in the project's root, adding the `nextapp` service.

Below the updated version:

```bash
services:
  nextapp:
    container_name: nextapp
    image: nextapp:1.0.0
    build:
      context: frontend
      dockerfile: Dockerfile
    ports:
      - "3000:3000"
    environment:
      - NEXT_PUBLIC_API_URL=http://localhost:4000
    depends_on:
      - flaskapp

  flaskapp:
    container_name: flaskapp
    image: flaskapp:1.0.0
    build:
      context: backend
      dockerfile: Dockerfile
    ports:
      - '4000:4000'
    restart: always
    environment:
      - DATABASE_URL=postgresql://postgres:postgres@db:5432/postgres
    depends_on:
      - db
      
  db:
    container_name: db
    image: postgres:13
    restart: always
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: postgres
    ports:
      - 5432:5432
    volumes:
      - pgdata:/var/lib/postgresql/data

volumes:
  pgdata:
```

And now, let's build the image and run the container:

```bash
docker compose build
docker compose up -d nextapp
```

Now, test the application by accessing to the `http://localhost:3000/` URL